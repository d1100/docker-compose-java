package com.empresa.hibernate;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "PEDIDOS")
public class Pedidos implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private Clientes cliente;
	private Date fecha_pedido;
	private Double iva_pedido;
	private Long numero_pedido;
	private Double otros_cargos_pedido;
	private Integer porcentaje_iva_pedido;
	private Double porte_pedido;
	private Double seguro_pedido;
	private Double total_bruto_pedido;
	private Double total_cargos_pedido;
	private Double total_factura_pedido;
	
	private Set<Linea_Pedido> linea_pedidoses;

	public Pedidos() {
	}

	public Pedidos(Clientes cliente, Date fecha_pedido, Double iva_pedido, Long numero_pedido, Double otros_cargos_pedido,
			Integer porcentaje_iva_pedido, Double porte_pedido, Double seguro_pedido, Double total_bruto_pedido,
			Double total_cargos_pedido, Double total_factura_pedido, Set<Linea_Pedido> linea_pedidoses) {
		this.cliente = cliente;
		this.fecha_pedido = fecha_pedido;
		this.iva_pedido = iva_pedido;
		this.numero_pedido = numero_pedido;
		this.otros_cargos_pedido = otros_cargos_pedido;
		this.porcentaje_iva_pedido = porcentaje_iva_pedido;
		this.porte_pedido = porte_pedido;
		this.seguro_pedido = seguro_pedido;
		this.total_bruto_pedido = total_bruto_pedido;
		this.total_cargos_pedido = total_cargos_pedido;
		this.total_factura_pedido = total_factura_pedido;
		this.linea_pedidoses = linea_pedidoses;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CODIGO_CLIENTE")
	public Clientes getCliente() {
		return cliente;
	}

	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_PEDIDO", length = 7)
	public Date getFecha_pedido() {
		return fecha_pedido;
	}

	public void setFecha_pedido(Date fecha_pedido) {
		this.fecha_pedido = fecha_pedido;
	}

	@Column(name = "IVA_PEDIDO", unique = false, nullable = false, precision = 11, scale = 2)
	public Double getIva_pedido() {
		return iva_pedido;
	}

	public void setIva_pedido(Double iva_pedido) {
		this.iva_pedido = iva_pedido;
	}

	@Id
	@Column(name = "NUMERO_PEDIDO", unique = true, nullable = false, precision = 5, scale = 0)
	public Long getNumero_pedido() {
		return numero_pedido;
	}

	public void setNumero_pedido(Long numero_pedido) {
		this.numero_pedido = numero_pedido;
	}

	@Column(name = "OTROS_CARGOS_PEDIDO", unique = false, nullable = false, precision = 11, scale = 2)
	public Double getOtros_cargos_pedido() {
		return otros_cargos_pedido;
	}

	public void setOtros_cargos_pedido(Double otros_cargos_pedido) {
		this.otros_cargos_pedido = otros_cargos_pedido;
	}

	@Column(name = "PORCENTAJE_IVA_PEDIDO", unique = false, nullable = false, precision = 2, scale = 0)
	public Integer getPorcentaje_iva_pedido() {
		return porcentaje_iva_pedido;
	}

	public void setPorcentaje_iva_pedido(Integer porcentaje_iva_pedido) {
		this.porcentaje_iva_pedido = porcentaje_iva_pedido;
	}

	@Column(name = "PORTE_PEDIDO", unique = false, nullable = false, precision = 11, scale = 2)
	public Double getPorte_pedido() {
		return porte_pedido;
	}

	public void setPorte_pedido(Double porte_pedido) {
		this.porte_pedido = porte_pedido;
	}

	@Column(name = "SEGURO_PEDIDO", unique = false, nullable = false, precision = 11, scale = 2)
	public Double getSeguro_pedido() {
		return seguro_pedido;
	}

	public void setSeguro_pedido(Double seguro_pedido) {
		this.seguro_pedido = seguro_pedido;
	}

	@Column(name = "TOTAL_BRUTO_PEDIDO", unique = false, nullable = false, precision = 11, scale = 2)
	public Double getTotal_bruto_pedido() {
		return total_bruto_pedido;
	}

	public void setTotal_bruto_pedido(Double total_bruto_pedido) {
		this.total_bruto_pedido = total_bruto_pedido;
	}

	@Column(name = "TOTAL_CARGOS_PEDIDO", unique = false, nullable = false, precision = 11, scale = 2)
	public Double getTotal_cargos_pedido() {
		return total_cargos_pedido;
	}

	public void setTotal_cargos_pedido(Double total_cargos_pedido) {
		this.total_cargos_pedido = total_cargos_pedido;
	}

	@Column(name = "TOTAL_FACTURA_PEDIDO", unique = false, nullable = false, precision = 11, scale = 2)
	public Double getTotal_factura_pedido() {
		return total_factura_pedido;
	}

	public void setTotal_factura_pedido(Double total_factura_pedido) {
		this.total_factura_pedido = total_factura_pedido;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "pedido")
	public Set<Linea_Pedido> getLinea_pedidoses() {
		return linea_pedidoses;
	}

	public void setLinea_pedidoses(Set<Linea_Pedido> linea_pedidoses) {
		this.linea_pedidoses = linea_pedidoses;
	}

	
	@Override
	public String toString() {
		return "Pedidos [cliente=" + cliente + ", fecha_pedido=" + fecha_pedido + ", iva_pedido="
				+ iva_pedido + ", numero_pedido=" + numero_pedido + ", otros_cargos_pedido=" + otros_cargos_pedido
				+ ", porcentaje_iva_pedido=" + porcentaje_iva_pedido + ", porte_pedido=" + porte_pedido
				+ ", seguro_pedido=" + seguro_pedido + ", total_bruto_pedido=" + total_bruto_pedido
				+ ", total_cargos_pedido=" + total_cargos_pedido + ", total_factura_pedido=" + total_factura_pedido
				+ "]";
	}
}