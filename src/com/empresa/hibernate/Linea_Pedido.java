package com.empresa.hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "LINEA_PEDIDO")
public class Linea_Pedido implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private Long codigo_linea_pedido;
	private Articulos articulo;
	private Pedidos pedido;
	private Double precio_unidad_articulo;
	private Integer numero_unidades_articulo;
	private Double porcentaje_descuento;

	public Linea_Pedido() {
	}

	public Linea_Pedido(Long codigo_linea_pedido, Articulos articulo, Pedidos pedido, Double precio_unidad_articulo, Integer numero_unidades_articulo,
			Double porcentaje_descuento) {
		this.codigo_linea_pedido = codigo_linea_pedido;
		this.articulo = articulo;
		this.pedido = pedido;
		this.numero_unidades_articulo = numero_unidades_articulo;
		this.porcentaje_descuento = porcentaje_descuento;
		this.precio_unidad_articulo = precio_unidad_articulo;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CODIGO_ARTICULO")
	public Articulos getArticulo() {
		return articulo;
	}

	public void setArticulo(Articulos articulo) {
		this.articulo = articulo;
	}

	@Id
	@Column(name = "CODIGO_LINEA_PEDIDO", unique = true, nullable = false, precision = 10, scale = 0)
	public Long getCodigo_linea_pedido() {
		return codigo_linea_pedido;
	}

	public void setCodigo_linea_pedido(Long codigo_linea_pedido) {
		this.codigo_linea_pedido = codigo_linea_pedido;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "NUMERO_PEDIDO")
	public Pedidos getPedido() {
		return pedido;
	}

	public void setPedido(Pedidos pedido) {
		this.pedido = pedido;
	}

	@Column(name = "NUMERO_UNIDADES_ARTICULO", unique = false, nullable = true, precision = 5, scale = 0)
	public Integer getNumero_unidades_articulo() {
		return numero_unidades_articulo;
	}

	public void setNumero_unidades_articulo(Integer numero_unidades_articulo) {
		this.numero_unidades_articulo = numero_unidades_articulo;
	}

	@Column(name = "PORCENTAJE_DESCUENTO", unique = false, nullable = true, precision = 4, scale = 2)
	public Double getPorcentaje_descuento() {
		return porcentaje_descuento;
	}

	public void setPorcentaje_descuento(Double porcentaje_descuento) {
		this.porcentaje_descuento = porcentaje_descuento;
	}

	@Column(name = "PRECIO_UNIDAD_ARTICULO", unique = false, nullable = true, precision = 11, scale = 2)
	public Double getPrecio_unidad_articulo() {
		return precio_unidad_articulo;
	}

	public void setPrecio_unidad_articulo(Double precio_unidad_articulo) {
		this.precio_unidad_articulo = precio_unidad_articulo;
	}

	@Override
	public String toString() {
		return "Linea_Pedido [articulo=" + articulo + ", codigo_linea_pedido=" + codigo_linea_pedido
				+ ", pedido=" + pedido + ", numero_unidades_articulo=" + numero_unidades_articulo
				+ ", porcentaje_descuento=" + porcentaje_descuento + ", precio_unidad_articulo="
				+ precio_unidad_articulo + "]";
	}
}