package com.empresa.hibernate.dao;

import java.util.List;

import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import static org.hibernate.criterion.Example.create;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.empresa.hibernate.Clientes;

@Repository("cliente_dao")
@Scope("prototype")
public class ClientesDAO {
	private static final Logger log = LoggerFactory.getLogger(ClientesDAO.class);
	// property constants
	public static final String NOMBRE_CLIENTE = "nombre_cliente";
	public static final String TIPO_EMPRESA = "tipo_empresa";
	public static final String CIF_CLIENTE = "cif_cliente";
	public static final String NIF_CLIENTE = "nif_cliente";
	public static final String NOMBRE_COMERCIAL_CLIENTE = "nombre_comercial_cliente";
	public static final String FECHA_ALTA_CLIENTE = "fecha_alta_cliente";
	public static final String FECHA_BAJA_CLIENTE = "fecha_baja_cliente";
	public static final String CALLE_CLIENTE = "calle_cliente";
	public static final String CIUDAD_CLIENTE = "ciudad_cliente";
	public static final String CP_CLIENTE = "cp_cliente";
	public static final String PROVINCIA_CLIENTE = "provincia_cliente";
	public static final String TELEFONO_CLIENTE = "telefono_cliente";
	public static final String FAX_CLIENTE = "fax_cliente";
	public static final String EMAIL_CLIENTE = "email_cliente";
	public static final String CONTADO_CLIENTE = "contado_cliente";
	public static final String CREDITO_CLIENTE = "credito_cliente";
	public static final String CIFRA_VENTAS_CLIENTE = "cifra_ventas_cliente";
	public static final String BENEFICIOS_CLIENTE = "beneficios_cliente";
	public static final String PREPAGO_CLIENTE = "prepago_cliente";
	public static final String CAPITAL_CLIENTE = "capital_cliente";
	public static final String INMOVILIZADO_CLIENTE = "inmovilizado_cliente";
	public static final String PASIVO_CLIENTE = "pasivo_cliente";
	public static final String ACTIVO_CLIENTE = "activo_cliente";
	public static final String A�O_BALANCE_CLIENTE = "a�o_balance_cliente";
	public static final String LIMITE_CLIENTE = "limite_cliente";
	public static final String PENDIENTE_CLIENTE = "pendiente_cliente";
	public static final String RESTO_CLIENTE = "resto_cliente";
	public static final String CALLE_ENVIO_CLIENTE = "calle_envio_cliente";
	public static final String CIUDAD_ENVIO_CLIENTE = "ciudad_envio_cliente";
	public static final String CP_ENVIO_CLIENTE = "cp_envio_cliente";
	public static final String PROVINCIA_ENVIO_CLIENTE = "provincia_envio_cliente";
	public static final String TELEFONO_ENVIO_CLIENTE = "telefono_envio_cliente";
	public static final String FAX_ENVIO_CLIENTE = "fax_envio_cliente";
	public static final String EMAIL_ENVIO_CLIENTE = "email_envio_cliente";
	public static final String OBSERVACIONES_CLIENTE = "observaciones_cliente";
	public static final String ESTADO_CLIENTE = "estado_cliente";

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	protected void initDao() {
		// do nothing
	}

	public void save(Clientes transientInstance) {
		log.debug("saving Clientes instance");
		try {
			getCurrentSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Clientes persistentInstance) {
		log.debug("deleting Clientes instance");
		try {
			getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Clientes findById(java.lang.Long codigoCliente) {
		log.debug("getting Clientes instance with codigoCliente: " + codigoCliente);
		try {
			Clientes instance = (Clientes) getCurrentSession().get(
					"com.empresa.hibernate.Clientes", codigoCliente);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Clientes> findByExample(Clientes instance) {
		log.debug("finding Clientes instance by example");
		try {
			List<Clientes> results = (List<Clientes>) getCurrentSession()
					.createCriteria("com.empresa.hibernate.Clientes")
					.add(create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Clientes instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Clientes as model where model."
					+ propertyName + "= ?";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByNombreCliente(Object nombreCliente) {
		return findByProperty(NOMBRE_CLIENTE, nombreCliente);
	}

	public List findByTipoEmpresa(Object tipoEmpresa) {
		return findByProperty(TIPO_EMPRESA, tipoEmpresa);
	}

	public List findByCifCliente(Object cifCliente) {
		return findByProperty(CIF_CLIENTE, cifCliente);
	}

	public List findByNifCliente(Object nifCliente) {
		return findByProperty(NIF_CLIENTE, nifCliente);
	}

	public List findByNombreComercialCliente(Object nombreComercialCliente) {
		return findByProperty(NOMBRE_COMERCIAL_CLIENTE, nombreComercialCliente);
	}

	public List findByFechaAltaCliente(Object fechaAltaCliente) {
		return findByProperty(FECHA_ALTA_CLIENTE, fechaAltaCliente);
	}

	public List findByFechaBajaCliente(Object fechaBajaCliente) {
		return findByProperty(FECHA_BAJA_CLIENTE, fechaBajaCliente);
	}

	public List findByCalleCliente(Object calleCliente) {
		return findByProperty(CALLE_CLIENTE, calleCliente);
	}

	public List findByCiudadCliente(Object ciudadCliente) {
		return findByProperty(CIUDAD_CLIENTE, ciudadCliente);
	}

	public List findByCpCliente(Object cpCliente) {
		return findByProperty(CP_CLIENTE, cpCliente);
	}

	public List findByProvinciaCliente(Object provinciaCliente) {
		return findByProperty(PROVINCIA_CLIENTE, provinciaCliente);
	}

	public List findByTelefonoCliente(Object telefonoCliente) {
		return findByProperty(TELEFONO_CLIENTE, telefonoCliente);
	}

	public List findByFaxCliente(Object faxCliente) {
		return findByProperty(FAX_CLIENTE, faxCliente);
	}

	public List findByEmailCliente(Object emailCliente) {
		return findByProperty(EMAIL_CLIENTE, emailCliente);
	}

	public List findByContadoCliente(Object contadoCliente) {
		return findByProperty(CONTADO_CLIENTE, contadoCliente);
	}

	public List findByCreditoCliente(Object creditoCliente) {
		return findByProperty(CREDITO_CLIENTE, creditoCliente);
	}

	public List findByCifraVentasCliente(Object cifraVentasCliente) {
		return findByProperty(CIFRA_VENTAS_CLIENTE, cifraVentasCliente);
	}

	public List findByBeneficiosCliente(Object beneficiosCliente) {
		return findByProperty(BENEFICIOS_CLIENTE, beneficiosCliente);
	}

	public List findByPrepagoCliente(Object prepagoCliente) {
		return findByProperty(PREPAGO_CLIENTE, prepagoCliente);
	}

	public List findByCapitalCliente(Object capitalCliente) {
		return findByProperty(CAPITAL_CLIENTE, capitalCliente);
	}

	public List findByInmovilizacionCliente(Object inmovilizacionCliente) {
		return findByProperty(INMOVILIZADO_CLIENTE, inmovilizacionCliente);
	}

	public List findByPasivoCliente(Object pasivoCliente) {
		return findByProperty(PASIVO_CLIENTE, pasivoCliente);
	}

	public List findByActivoCliente(Object activoCliente) {
		return findByProperty(ACTIVO_CLIENTE, activoCliente);
	}

	public List findByA�oBalanceCliente(Object a�oBalanceCliente) {
		return findByProperty(A�O_BALANCE_CLIENTE, a�oBalanceCliente);
	}

	public List findByLimiteCliente(Object limiteCliente) {
		return findByProperty(LIMITE_CLIENTE, limiteCliente);
	}

	public List findByPendienteCliente(Object pendienteCliente) {
		return findByProperty(PENDIENTE_CLIENTE, pendienteCliente);
	}

	public List findByRestoCliente(Object restoCliente) {
		return findByProperty(RESTO_CLIENTE, restoCliente);
	}

	public List findByCalleEnvioCliente(Object calleEnvioCliente) {
		return findByProperty(CALLE_ENVIO_CLIENTE, calleEnvioCliente);
	}

	public List findByCiudadEnvioCliente(Object ciudadEnvioCliente) {
		return findByProperty(CIUDAD_ENVIO_CLIENTE, ciudadEnvioCliente);
	}

	public List findByCpEnvioCliente(Object cpEnvioCliente) {
		return findByProperty(CP_ENVIO_CLIENTE, cpEnvioCliente);
	}

	public List findByProvinciaEnvioCliente(Object provinciaEnvioCliente) {
		return findByProperty(PROVINCIA_ENVIO_CLIENTE, provinciaEnvioCliente);
	}

	public List findByTelefonoEnvioCliente(Object telefonoEnvioCliente) {
		return findByProperty(TELEFONO_ENVIO_CLIENTE, telefonoEnvioCliente);
	}

	public List findByFaxEnvioCliente(Object faxEnvioCliente) {
		return findByProperty(FAX_ENVIO_CLIENTE, faxEnvioCliente);
	}

	public List findByEmailEnvioCliente(Object emailEnvioCliente) {
		return findByProperty(EMAIL_ENVIO_CLIENTE, emailEnvioCliente);
	}

	public List findByObservacionesCliente(Object observacionesCliente) {
		return findByProperty(OBSERVACIONES_CLIENTE, observacionesCliente);
	}

	public List findByEstadoCliente(Object estadoCliente) {
		return findByProperty(ESTADO_CLIENTE, estadoCliente);
	}

	public List findAll() {
		log.debug("finding all Clientes instances");
		try {
			String queryString = "from Clientes";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Clientes merge(Clientes detachedInstance) {
		log.debug("merging Clientes instance");
		try {
			Clientes result = (Clientes) getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Clientes instance) {
		log.debug("attaching dirty Clientes instance");
		try {
			getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Clientes instance) {
		log.debug("attaching clean Clientes instance");
		try {
			getCurrentSession().buildLockRequest(LockOptions.NONE).lock(
					instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static ClientesDAO getFromApplicationContext(ApplicationContext ctx) {
		return (ClientesDAO) ctx.getBean("ClientesDAO");
	}
}