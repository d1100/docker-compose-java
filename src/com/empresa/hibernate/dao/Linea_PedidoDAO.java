package com.empresa.hibernate.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import static org.hibernate.criterion.Example.create;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.empresa.hibernate.Linea_Pedido;
import com.empresa.hibernate.Pedidos;

@Repository("linea_pedido_dao")
@Scope("prototype")
public class Linea_PedidoDAO {
	private static final Logger log = LoggerFactory.getLogger(Linea_PedidoDAO.class);
	// property constants
	public static final String PRECIO_UNIDAD_ARTICULO = "precio_unidad_articulo";
	public static final String NUMERO_UNIDADES_ARTICULO = "numero_unidades_articulo";
	public static final String PORCENTAJE_DESCUENTO = "porcentaje_descuento";

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	protected void initDao() {
		// do nothing
	}
	
	public Long getMax() {
		log.debug("getting the Max function");
		try {
			String queryString = "select max(lp.codigo_linea_pedido) as codigo_linea_pedido from Linea_Pedido lp";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return (Long) queryObject.list().get(0);
		} catch (RuntimeException re) {
			log.error("get the max value failed", re);
			throw re;
		}
	}
	
	public List<Linea_Pedido> consultar_Lista(Long numero_pedido){
		Criteria consulta = getCurrentSession().createCriteria(Pedidos.class);
		consulta.setFetchMode("pedido", FetchMode.EAGER);
		consulta.setFetchMode("pedido.pedidoses", FetchMode.EAGER);
		consulta.add(Restrictions.idEq(numero_pedido));
		Pedidos pedido = (Pedidos) consulta.uniqueResult();
		List<Linea_Pedido> lista = new ArrayList<Linea_Pedido>(0);
		if (pedido != null) {
			Set<Linea_Pedido> set_pedidos = pedido.getLinea_pedidoses();
			if (!set_pedidos.isEmpty()) {
				lista.addAll(set_pedidos);
			}
		}
		return lista;
	}

	public void save(Linea_Pedido transientInstance) {
		log.debug("saving Linea_Pedido instance");
		try {
			getCurrentSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Linea_Pedido persistentInstance) {
		log.debug("deleting Linea_Pedido instance");
		try {
			getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Linea_Pedido findById(java.lang.Long codigoLineaPedido) {
		log.debug("getting Linea_Pedido instance with codigoLineaPedido: " + codigoLineaPedido);
		try {
			Linea_Pedido instance = (Linea_Pedido) getCurrentSession().get(
					"com.empresa.hibernate.Linea_Pedido", codigoLineaPedido);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Linea_Pedido> findByExample(Linea_Pedido instance) {
		log.debug("finding Linea_Pedido instance by example");
		try {
			List<Linea_Pedido> results = (List<Linea_Pedido>) getCurrentSession()
					.createCriteria("com.empresa.hibernate.Linea_Pedido")
					.add(create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Linea_Pedido instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Linea_Pedido as model where model."
					+ propertyName + "= ?";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByPrecioUnidadArticulo(Object precioUnidadArticulo) {
		return findByProperty(PRECIO_UNIDAD_ARTICULO, precioUnidadArticulo);
	}

	public List findByNumeroUnidadesArticulo(Object numeroUnidadesArticulo) {
		return findByProperty(NUMERO_UNIDADES_ARTICULO, numeroUnidadesArticulo);
	}

	public List findByPorcentajeDescuento(Object porcentajeDescuento) {
		return findByProperty(PORCENTAJE_DESCUENTO, porcentajeDescuento);
	}

	public List findAll() {
		log.debug("finding all Linea_Pedido instances");
		try {
			String queryString = "from Linea_Pedido";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Linea_Pedido merge(Linea_Pedido detachedInstance) {
		log.debug("merging Linea_Pedido instance");
		try {
			Linea_Pedido result = (Linea_Pedido) getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Linea_Pedido instance) {
		log.debug("attaching dirty Linea_Pedido instance");
		try {
			getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Linea_Pedido instance) {
		log.debug("attaching clean Linea_Pedido instance");
		try {
			getCurrentSession().buildLockRequest(LockOptions.NONE).lock(
					instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static Linea_PedidoDAO getFromApplicationContext(ApplicationContext ctx) {
		return (Linea_PedidoDAO) ctx.getBean("Linea_PedidoDAO");
	}
}