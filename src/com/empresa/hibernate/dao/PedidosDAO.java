package com.empresa.hibernate.dao;

import java.util.List;

import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import static org.hibernate.criterion.Example.create;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.empresa.hibernate.Pedidos;

@Repository("pedido_dao")
@Scope("prototype")
public class PedidosDAO {
	private static final Logger log = LoggerFactory.getLogger(PedidosDAO.class);
	// property constants
	public static final String TOTAL_FACTURA_PEDIDO = "total_factura_pedido";
	public static final String FECHA_PEDIDO = "fecha_pedido";
	public static final String PORTE_PEDIDO = "porte_pedido";
	public static final String SEGURO_PEDIDO = "seguro_pedido";
	public static final String OTROS_CARGOS_PEDIDO = "otros_cargos_pedido";
	public static final String TOTAL_CARGOS_PEDIDO = "total_cargos_pedido";
	public static final String TOTAL_BRUTO_PEDIDO = "total_bruto_pedido";
	public static final String PORCENTAJE_IVA_PEDIDO = "porcentaje_iva_pedido";
	public static final String IVA_PEDIDO = "iva_pedido";

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	protected void initDao() {
		// do nothing
	}
	
	public Long getMax() {
		log.debug("getting the Max function");
		try {
			String queryString = "select max(p.numero_pedido)as numero_pedido from Pedidos p";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return (Long) queryObject.list().get(0);
		} catch (RuntimeException re) {
			log.error("get the max value failed", re);
			throw re;
		}
	}

	public void save(Pedidos transientInstance) {
		log.debug("saving Pedidos instance");
		try {
			getCurrentSession().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re) {
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(Pedidos persistentInstance) {
		log.debug("deleting Pedidos instance");
		try {
			getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Pedidos findById(java.lang.Long numeroPedido) {
		log.debug("getting Pedidos instance with numeroPedido: " + numeroPedido);
		try {
			Pedidos instance = (Pedidos) getCurrentSession().get(
					"com.empresa.hibernate.Pedidos", numeroPedido);
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Pedidos> findByExample(Pedidos instance) {
		log.debug("finding Pedidos instance by example");
		try {
			List<Pedidos> results = (List<Pedidos>) getCurrentSession()
					.createCriteria("com.empresa.hibernate.Pedidos")
					.add(create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value) {
		log.debug("finding Pedidos instance with property: " + propertyName
				+ ", value: " + value);
		try {
			String queryString = "from Pedidos as model where model."
					+ propertyName + "= ?";
			Query queryObject = getCurrentSession().createQuery(queryString);
			queryObject.setParameter(0, value);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByTotalFacturaPedido(Object totalFacturaPedido) {
		return findByProperty(TOTAL_FACTURA_PEDIDO, totalFacturaPedido);
	}

	public List findByFechaPedido(Object fechaPedido) {
		return findByProperty(FECHA_PEDIDO, fechaPedido);
	}
	
	public List findByPortePedido(Object portePedido) {
		return findByProperty(PORTE_PEDIDO, portePedido);
	}
	
	public List findBySeguroPedido(Object seguroPedido) {
		return findByProperty(SEGURO_PEDIDO, seguroPedido);
	}
	
	public List findByOtrosCargosPedido(Object otrosCargosPedido) {
		return findByProperty(OTROS_CARGOS_PEDIDO, otrosCargosPedido);
	}
	
	public List findByTotalCargosPedido(Object totalCargosPedido) {
		return findByProperty(TOTAL_CARGOS_PEDIDO, totalCargosPedido);
	}
	
	public List findByTotalBrutoPedido(Object totalBrutoPedido) {
		return findByProperty(TOTAL_BRUTO_PEDIDO, totalBrutoPedido);
	}
	
	public List findByPorcentajeIvaPedido(Object porcentajeIvaPedido) {
		return findByProperty(PORCENTAJE_IVA_PEDIDO, porcentajeIvaPedido);
	}
	
	public List findByIvaPedido(Object ivaPedido) {
		return findByProperty(IVA_PEDIDO, ivaPedido);
	}

	public List findAll() {
		log.debug("finding all Pedidos instances");
		try {
			String queryString = "from Pedidos";
			Query queryObject = getCurrentSession().createQuery(queryString);
			return queryObject.list();
		} catch (RuntimeException re) {
			log.error("find all failed", re);
			throw re;
		}
	}

	public Pedidos merge(Pedidos detachedInstance) {
		log.debug("merging Pedidos instance");
		try {
			Pedidos result = (Pedidos) getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(Pedidos instance) {
		log.debug("attaching dirty Pedidos instance");
		try {
			getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Pedidos instance) {
		log.debug("attaching clean Pedidos instance");
		try {
			getCurrentSession().buildLockRequest(LockOptions.NONE).lock(
					instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public static PedidosDAO getFromApplicationContext(ApplicationContext ctx) {
		return (PedidosDAO) ctx.getBean("PedidosDAO");
	}
}