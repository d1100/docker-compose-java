package com.empresa.hibernate.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("estadoConverter")
public class EstadoConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        return string;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        String estado = "";
        
        if (o != null){
            estado = (String) o;
            switch(estado){
                case "AC":
                    estado = "1. Estado: AC";
                    break;
                case "BA":
                    estado = "2. Estado: BA";
                    break;
                case "MO":
                    estado = "3. Estado: MO";
                    break;
            }
        }
        return estado;
    }    
}