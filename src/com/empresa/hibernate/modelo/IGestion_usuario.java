package com.empresa.hibernate.modelo;

import java.util.List;

import com.empresa.hibernate.Usuarios;

public interface IGestion_usuario {

	public Usuarios consultar_PorNombre(String nombre_usuario);

	public abstract List<Usuarios> consultar_Todos();
	
	public Usuarios iniciarSesion(Usuarios instance);

}