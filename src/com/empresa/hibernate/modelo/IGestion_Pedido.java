package com.empresa.hibernate.modelo;

import java.util.List;

import com.empresa.hibernate.Pedidos;

public interface IGestion_Pedido {

	public Pedidos consultar_PorCodigo(Long numeroPedido);

	public abstract List<Pedidos> consultar_Todos();

	public abstract void modificion_Pedido(Pedidos pedido);

	public abstract void baja_Pedido(Pedidos pedido);

	public abstract void alta_Pedido(Pedidos pedido);
	
	public abstract Long getMax();

}