package com.empresa.hibernate.modelo;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.empresa.hibernate.Articulos;
import com.empresa.hibernate.dao.ArticulosDAO;

@Component("gestion_articulo")
@Scope("prototype")
public class Gestion_Articulo implements IGestion_Articulo {

	private ArticulosDAO articulo_dao;

	// ***************** CONSULTAS DE USUARIOS
	@Override
	@Transactional(readOnly = true)
	public Articulos consultar_PorCodigo(Long codigoArticulo) {
		return articulo_dao.findById(codigoArticulo);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Articulos> consultar_Todos() {
		List<Articulos> lista = articulo_dao.findAll();
		return lista;
	}

	// *************** OPERACIONES CRUD USUARIOS

	@Override
	@Transactional
	public void alta_Articulo(Articulos articulo) {
		articulo_dao.save(articulo);
	}

	@Override
	@Transactional
	public void baja_Articulo(Articulos articulo) {
		articulo_dao.delete(articulo);
	}

	@Override
	@Transactional
	public void modificion_Articulo(Articulos articulo) {
		articulo_dao.attachDirty(articulo);
	}

	// ACCESORES PARA SPRING
	public void setArticulo_dao(ArticulosDAO articulo_dao) {
		this.articulo_dao = articulo_dao;
	}
}