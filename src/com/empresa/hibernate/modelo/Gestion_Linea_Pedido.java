package com.empresa.hibernate.modelo;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.empresa.hibernate.Linea_Pedido;
import com.empresa.hibernate.dao.Linea_PedidoDAO;

@Component("gestion_linea_pedido")
@Scope("prototype")
public class Gestion_Linea_Pedido implements IGestion_Linea_Pedido {

	private Linea_PedidoDAO linea_pedido_dao;

	// ***************** CONSULTAS DE USUARIOS
	@Override
	@Transactional(readOnly = true)
	public Linea_Pedido consultar_PorCodigo(Long codigoLineaPedido) {
		return linea_pedido_dao.findById(codigoLineaPedido);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Linea_Pedido> consultar_Todos() {
		List<Linea_Pedido> lista = linea_pedido_dao.findAll();
		return lista;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Linea_Pedido> consultar_Lista(Long numero_pedido) {
		List<Linea_Pedido> lista = linea_pedido_dao.consultar_Lista(numero_pedido);
		return lista;
	}

	// *************** OPERACIONES CRUD USUARIOS

	@Override
	@Transactional
	public void alta_Linea_Pedido(Linea_Pedido linea_pedido) {
		linea_pedido_dao.save(linea_pedido);
	}

	@Override
	@Transactional
	public void baja_Linea_Pedido(Linea_Pedido linea_pedido) {
		linea_pedido_dao.delete(linea_pedido);
	}

	@Override
	@Transactional
	public void modificion_Linea_Pedido(Linea_Pedido linea_pedido) {
		linea_pedido_dao.attachDirty(linea_pedido);
	}

	// ACCESORES PARA SPRING
	public void setLinea_pedido_dao(Linea_PedidoDAO linea_pedido_dao) {
		this.linea_pedido_dao = linea_pedido_dao;
	}

	@Override
	@Transactional(readOnly = true)
	public Long getMax() {
		return linea_pedido_dao.getMax();
	}
}