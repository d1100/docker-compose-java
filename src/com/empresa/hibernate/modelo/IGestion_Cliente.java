package com.empresa.hibernate.modelo;

import java.util.List;

import com.empresa.hibernate.Clientes;

public interface IGestion_Cliente {

	public Clientes consultar_PorCodigo(Long codigoCliente);

	public abstract List<Clientes> consultar_Todos();

	public abstract void modificion_Cliente(Clientes cliente);

	public abstract void baja_Cliente(Clientes cliente);

	public abstract void alta_Cliente(Clientes cliente);

}