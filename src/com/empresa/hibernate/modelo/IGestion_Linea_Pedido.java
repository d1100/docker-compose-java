package com.empresa.hibernate.modelo;

import java.util.List;

import com.empresa.hibernate.Linea_Pedido;

public interface IGestion_Linea_Pedido {

	public Linea_Pedido consultar_PorCodigo(Long codigoLineaPedido);

	public abstract List<Linea_Pedido> consultar_Todos();

	public abstract void modificion_Linea_Pedido(Linea_Pedido linea_pedido);

	public abstract void baja_Linea_Pedido(Linea_Pedido linea_pedido);

	public abstract void alta_Linea_Pedido(Linea_Pedido linea_pedido);
	
	public abstract Long getMax();
	
	public abstract List<Linea_Pedido> consultar_Lista(Long numero_pedido);

}