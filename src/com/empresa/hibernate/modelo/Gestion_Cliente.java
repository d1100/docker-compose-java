package com.empresa.hibernate.modelo;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.empresa.hibernate.Clientes;
import com.empresa.hibernate.dao.ClientesDAO;

@Component("gestion_cliente")
@Scope("prototype")
public class Gestion_Cliente implements IGestion_Cliente {

	private ClientesDAO cliente_dao;

	// ***************** CONSULTAS DE USUARIOS
	@Override
	@Transactional(readOnly = true)
	public Clientes consultar_PorCodigo(Long codigoCliente) {
		return cliente_dao.findById(codigoCliente);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Clientes> consultar_Todos() {
		List<Clientes> lista = cliente_dao.findAll();
		return lista;
	}

	// *************** OPERACIONES CRUD USUARIOS

	@Override
	@Transactional
	public void alta_Cliente(Clientes cliente) {
		cliente_dao.save(cliente);
	}

	@Override
	@Transactional
	public void baja_Cliente(Clientes cliente) {
		cliente_dao.delete(cliente);
	}

	@Override
	@Transactional
	public void modificion_Cliente(Clientes cliente) {
		cliente_dao.attachDirty(cliente);
	}

	// ACCESORES PARA SPRING
	public void setCliente_dao(ClientesDAO cliente_dao) {
		this.cliente_dao = cliente_dao;
	}
}