package com.empresa.hibernate.modelo;

import java.util.List;

import com.empresa.hibernate.Articulos;

public interface IGestion_Articulo {

	public Articulos consultar_PorCodigo(Long codigoArticulo);

	public abstract List<Articulos> consultar_Todos();

	public abstract void modificion_Articulo(Articulos articulo);

	public abstract void baja_Articulo(Articulos articulo);

	public abstract void alta_Articulo(Articulos articulo);

}