package com.empresa.hibernate.modelo;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.empresa.hibernate.Pedidos;
import com.empresa.hibernate.dao.PedidosDAO;

@Component("gestion_pedido")
@Scope("prototype")
public class Gestion_Pedido implements IGestion_Pedido {

	private PedidosDAO pedido_dao;

	// ***************** CONSULTAS DE USUARIOS
	@Override
	@Transactional(readOnly = true)
	public Pedidos consultar_PorCodigo(Long numeroPedido) {
		return pedido_dao.findById(numeroPedido);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Pedidos> consultar_Todos() {
		List<Pedidos> lista = pedido_dao.findAll();
		return lista;
	}
	
	@Override
	@Transactional(readOnly = true)
	public Long getMax() {
		return pedido_dao.getMax();
	}

	// *************** OPERACIONES CRUD USUARIOS

	@Override
	@Transactional
	public void alta_Pedido(Pedidos pedido) {
		pedido_dao.save(pedido);
	}

	@Override
	@Transactional
	public void baja_Pedido(Pedidos pedido) {
		pedido_dao.delete(pedido);
	}

	@Override
	@Transactional
	public void modificion_Pedido(Pedidos pedido) {
		pedido_dao.attachDirty(pedido);
	}

	// ACCESORES PARA SPRING
	public void setPedido_dao(PedidosDAO pedido_dao) {
		this.pedido_dao = pedido_dao;
	}
}