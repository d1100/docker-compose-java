package com.empresa.hibernate;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ARTICULOS")
public class Articulos implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private Long codigo_articulo;
	private String descripcion_articulo;
	private Double precio_unidad_articulo;
	private Integer cantidad;
	
	private Set<Linea_Pedido> linea_pedidoses;
	
	public Articulos() {
		
	}

	public Articulos(Long codigo_articulo, String descripcion_articulo, Double precio_unidad_articulo, Integer cantidad, Set<Linea_Pedido> linea_pedidoses) {
		this.codigo_articulo = codigo_articulo;
		this.descripcion_articulo = descripcion_articulo;
		this.precio_unidad_articulo = precio_unidad_articulo;
		this.cantidad = cantidad;
		this.linea_pedidoses = linea_pedidoses;
	}

	@Id
	@Column(name = "CODIGO_ARTICULO", unique = true, nullable = false, precision = 5, scale = 0)
	public Long getCodigo_articulo() {
		return codigo_articulo;
	}

	public void setCodigo_articulo(Long codigo_articulo) {
		this.codigo_articulo = codigo_articulo;
	}

	@Column(name = "DESCRIPCION_ARTICULO", length = 40)
	public String getDescripcion_articulo() {
		return descripcion_articulo;
	}

	public void setDescripcion_articulo(String descripcion_articulo) {
		this.descripcion_articulo = descripcion_articulo;
	}

	@Column(name = "PRECIO_UNIDAD_ARTICULO", unique = false, nullable = true, precision = 11, scale = 2)
	public Double getPrecio_unidad_articulo() {
		return precio_unidad_articulo;
	}

	public void setPrecio_unidad_articulo(Double precio_unidad_articulo) {
		this.precio_unidad_articulo = precio_unidad_articulo;
	}

	@Column(name = "CANTIDAD", unique = false, nullable = true, precision = 5, scale = 0)
	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "articulo")
	public Set<Linea_Pedido> getLinea_pedidoses() {
		return linea_pedidoses;
	}

	public void setLinea_pedidoses(Set<Linea_Pedido> linea_pedidoses) {
		this.linea_pedidoses = linea_pedidoses;
	}

	@Override
	public String toString() {
		return "Articulos [codigo_articulo=" + codigo_articulo + ", descripcion_articulo=" + descripcion_articulo
				+ ", precio_unidad_articulo=" + precio_unidad_articulo + ", cantidad=" + cantidad + "]";
	}
}
