package com.empresa.hibernate;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CLIENTES")
public class Clientes implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private Long codigo_cliente;
	private String nombre_cliente;
	private Character tipo_empresa;
	private String cif_cliente;
	private String nif_cliente;
	private String nombre_comercial_cliente;
	private Date fecha_alta_cliente;
	private Date fecha_baja_cliente;
	private String calle_cliente;
	private String ciudad_cliente;
	private String cp_cliente;
	private String provincia_cliente;
	private String telefono_cliente;
	private String fax_cliente;
	private String email_cliente;
	private Character contado_cliente;
	private Character credito_cliente;
	private Double cifra_ventas_cliente;
	private Double beneficios_cliente;
	private Character prepago_cliente;
	private Double capital_cliente;
	private Double inmovilizado_cliente;
	private Double pasivo_cliente;
	private Double activo_cliente;
	private Date a�o_balance_cliente;
	private Double limite_cliente;
	private Double pendiente_cliente;
	private Double resto_cliente;
	private String calle_envio_cliente;
	private String ciudad_envio_cliente;
	private String cp_envio_cliente;
	private String provincia_envio_cliente;
	private String telefono_envio_cliente;
	private String fax_envio_cliente;
	private String email_envio_cliente;
	private String observaciones_cliente;
	private String estado_cliente;
	
	private Set<Pedidos> pedidoses;
	
	public Clientes() {
		
	}

	public Clientes(Long codigo_cliente, String nombre_cliente, Character tipo_empresa, String cif_cliente,
			String nif_cliente, String nombre_comercial_cliente, Date fecha_alta_cliente, Date fecha_baja_cliente,
			String calle_cliente, String ciudad_cliente, String cp_cliente, String provincia_cliente,
			String telefono_cliente, String fax_cliente, String email_cliente, Character contado_cliente,
			Character credito_cliente, Double cifra_ventas_cliente, Double beneficios_cliente,
			Character prepago_cliente, Double capital_cliente, Double inmovilizado_cliente, Double pasivo_cliente,
			Double activo_cliente, Date a�o_balance_cliente, Double limite_cliente, Double pendiente_cliente,
			Double resto_cliente, String calle_envio_cliente, String ciudad_envio_cliente, String cp_envio_cliente,
			String provincia_envio_cliente, String telefono_envio_cliente, String fax_envio_cliente,
			String email_envio_cliente, String observaciones_cliente, String estado_cliente, Set<Pedidos> pedidoses) {
		this.codigo_cliente = codigo_cliente;
		this.nombre_cliente = nombre_cliente;
		this.tipo_empresa = tipo_empresa;
		this.cif_cliente = cif_cliente;
		this.nif_cliente = nif_cliente;
		this.nombre_comercial_cliente = nombre_comercial_cliente;
		this.fecha_alta_cliente = fecha_alta_cliente;
		this.fecha_baja_cliente = fecha_baja_cliente;
		this.calle_cliente = calle_cliente;
		this.ciudad_cliente = ciudad_cliente;
		this.cp_cliente = cp_cliente;
		this.provincia_cliente = provincia_cliente;
		this.telefono_cliente = telefono_cliente;
		this.fax_cliente = fax_cliente;
		this.email_cliente = email_cliente;
		this.contado_cliente = contado_cliente;
		this.credito_cliente = credito_cliente;
		this.cifra_ventas_cliente = cifra_ventas_cliente;
		this.beneficios_cliente = beneficios_cliente;
		this.prepago_cliente = prepago_cliente;
		this.capital_cliente = capital_cliente;
		this.inmovilizado_cliente = inmovilizado_cliente;
		this.pasivo_cliente = pasivo_cliente;
		this.activo_cliente = activo_cliente;
		this.a�o_balance_cliente = a�o_balance_cliente;
		this.limite_cliente = limite_cliente;
		this.pendiente_cliente = pendiente_cliente;
		this.resto_cliente = resto_cliente;
		this.calle_envio_cliente = calle_envio_cliente;
		this.ciudad_envio_cliente = ciudad_envio_cliente;
		this.cp_envio_cliente = cp_envio_cliente;
		this.provincia_envio_cliente = provincia_envio_cliente;
		this.telefono_envio_cliente = telefono_envio_cliente;
		this.fax_envio_cliente = fax_envio_cliente;
		this.email_envio_cliente = email_envio_cliente;
		this.observaciones_cliente = observaciones_cliente;
		this.estado_cliente = estado_cliente;
		this.pedidoses = pedidoses;
	}

	@Id
	@Column(name = "CODIGO_CLIENTE", unique = true, nullable = false, precision = 5, scale = 0)
	public Long getCodigo_cliente() {
		return codigo_cliente;
	}

	public void setCodigo_cliente(Long codigo_cliente) {
		this.codigo_cliente = codigo_cliente;
	}

	@Column(name = "NOMBRE_CLIENTE", length = 35)
	public String getNombre_cliente() {
		return nombre_cliente;
	}

	public void setNombre_cliente(String nombre_cliente) {
		this.nombre_cliente = nombre_cliente;
	}

	@Column(name = "TIPO_EMPRESA", length = 1)
	public Character getTipo_empresa() {
		return tipo_empresa;
	}

	public void setTipo_empresa(Character tipo_empresa) {
		this.tipo_empresa = tipo_empresa;
	}

	@Column(name = "CIF_CLIENTE", length = 10)
	public String getCif_cliente() {
		return cif_cliente;
	}

	public void setCif_cliente(String cif_cliente) {
		this.cif_cliente = cif_cliente;
	}

	@Column(name = "NIF_CLIENTE", length = 10)
	public String getNif_cliente() {
		return nif_cliente;
	}

	public void setNif_cliente(String nif_cliente) {
		this.nif_cliente = nif_cliente;
	}

	@Column(name = "NOMBRE_COMERCIAL_CLIENTE", length = 40)
	public String getNombre_comercial_cliente() {
		return nombre_comercial_cliente;
	}

	public void setNombre_comercial_cliente(String nombre_comercial_cliente) {
		this.nombre_comercial_cliente = nombre_comercial_cliente;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_ALTA_CLIENTE", length = 7)
	public Date getFecha_alta_cliente() {
		return fecha_alta_cliente;
	}

	public void setFecha_alta_cliente(Date fecha_alta_cliente) {
		this.fecha_alta_cliente = fecha_alta_cliente;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FECHA_BAJA_CLIENTE", length = 7)
	public Date getFecha_baja_cliente() {
		return fecha_baja_cliente;
	}

	public void setFecha_baja_cliente(Date fecha_baja_cliente) {
		this.fecha_baja_cliente = fecha_baja_cliente;
	}

	@Column(name = "CALLE_CLIENTE", length = 40)
	public String getCalle_cliente() {
		return calle_cliente;
	}

	public void setCalle_cliente(String calle_cliente) {
		this.calle_cliente = calle_cliente;
	}

	@Column(name = "CIUDAD_CLIENTE", length = 35)
	public String getCiudad_cliente() {
		return ciudad_cliente;
	}

	public void setCiudad_cliente(String ciudad_cliente) {
		this.ciudad_cliente = ciudad_cliente;
	}

	@Column(name = "CP_CLIENTE", length = 5)
	public String getCp_cliente() {
		return cp_cliente;
	}

	public void setCp_cliente(String cp_cliente) {
		this.cp_cliente = cp_cliente;
	}

	@Column(name = "PROVINCIA_CLIENTE", length = 35)
	public String getProvincia_cliente() {
		return provincia_cliente;
	}

	public void setProvincia_cliente(String provincia_cliente) {
		this.provincia_cliente = provincia_cliente;
	}

	@Column(name = "TELEFONO_CLIENTE", length = 9)
	public String getTelefono_cliente() {
		return telefono_cliente;
	}

	public void setTelefono_cliente(String telefono_cliente) {
		this.telefono_cliente = telefono_cliente;
	}

	@Column(name = "FAX_CLIENTE", length = 9)
	public String getFax_cliente() {
		return fax_cliente;
	}

	public void setFax_cliente(String fax_cliente) {
		this.fax_cliente = fax_cliente;
	}

	@Column(name = "EMAIL_CLIENTE", length = 35)
	public String getEmail_cliente() {
		return email_cliente;
	}

	public void setEmail_cliente(String email_cliente) {
		this.email_cliente = email_cliente;
	}

	@Column(name = "CONTADO_CLIENTE", length = 1)
	public Character getContado_cliente() {
		return contado_cliente;
	}

	public void setContado_cliente(Character contado_cliente) {
		this.contado_cliente = contado_cliente;
	}

	@Column(name = "CREDITO_CLIENTE", length = 1)
	public Character getCredito_cliente() {
		return credito_cliente;
	}

	public void setCredito_cliente(Character credito_cliente) {
		this.credito_cliente = credito_cliente;
	}

	@Column(name = "CIFRA_VENTAS_CLIENTE", unique = false, nullable = true, precision = 11, scale = 2)
	public Double getCifra_ventas_cliente() {
		return cifra_ventas_cliente;
	}

	public void setCifra_ventas_cliente(Double cifra_ventas_cliente) {
		this.cifra_ventas_cliente = cifra_ventas_cliente;
	}

	@Column(name = "BENEFICIOS_CLIENTE", unique = false, nullable = true, precision = 11, scale = 2)
	public Double getBeneficios_cliente() {
		return beneficios_cliente;
	}

	public void setBeneficios_cliente(Double beneficios_cliente) {
		this.beneficios_cliente = beneficios_cliente;
	}

	@Column(name = "PREPAGO_CLIENTE", length = 1)
	public Character getPrepago_cliente() {
		return prepago_cliente;
	}

	public void setPrepago_cliente(Character prepago_cliente) {
		this.prepago_cliente = prepago_cliente;
	}

	@Column(name = "CAPITAL_CLIENTE", unique = false, nullable = true, precision = 11, scale = 2)
	public Double getCapital_cliente() {
		return capital_cliente;
	}

	public void setCapital_cliente(Double capital_cliente) {
		this.capital_cliente = capital_cliente;
	}

	@Column(name = "INMOVILIZADO_CLIENTE", unique = false, nullable = true, precision = 11, scale = 2)
	public Double getInmovilizado_cliente() {
		return inmovilizado_cliente;
	}

	public void setInmovilizado_cliente(Double inmovilizado_cliente) {
		this.inmovilizado_cliente = inmovilizado_cliente;
	}

	@Column(name = "PASIVO_CLIENTE", unique = false, nullable = true, precision = 11, scale = 2)
	public Double getPasivo_cliente() {
		return pasivo_cliente;
	}

	public void setPasivo_cliente(Double pasivo_cliente) {
		this.pasivo_cliente = pasivo_cliente;
	}

	@Column(name = "ACTIVO_CLIENTE", unique = false, nullable = true, precision = 11, scale = 2)
	public Double getActivo_cliente() {
		return activo_cliente;
	}

	public void setActivo_cliente(Double activo_cliente) {
		this.activo_cliente = activo_cliente;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "A�O_BALANCE_CLIENTE", length = 7)
	public Date getA�o_balance_cliente() {
		return a�o_balance_cliente;
	}

	public void setA�o_balance_cliente(Date a�o_balance_cliente) {
		this.a�o_balance_cliente = a�o_balance_cliente;
	}

	@Column(name = "LIMITE_CLIENTE", unique = false, nullable = true, precision = 11, scale = 2)
	public Double getLimite_cliente() {
		return limite_cliente;
	}

	public void setLimite_cliente(Double limite_cliente) {
		this.limite_cliente = limite_cliente;
	}

	@Column(name = "PENDIENTE_CLIENTE", unique = false, nullable = true, precision = 11, scale = 2)
	public Double getPendiente_cliente() {
		return pendiente_cliente;
	}

	public void setPendiente_cliente(Double pendiente_cliente) {
		this.pendiente_cliente = pendiente_cliente;
	}

	@Column(name = "RESTO_CLIENTE", unique = false, nullable = true, precision = 11, scale = 2)
	public Double getResto_cliente() {
		return resto_cliente;
	}

	public void setResto_cliente(Double resto_cliente) {
		this.resto_cliente = resto_cliente;
	}

	@Column(name = "CALLE_ENVIO_CLIENTE", length = 40)
	public String getCalle_envio_cliente() {
		return calle_envio_cliente;
	}

	public void setCalle_envio_cliente(String calle_envio_cliente) {
		this.calle_envio_cliente = calle_envio_cliente;
	}

	@Column(name = "CIUDAD_ENVIO_CLIENTE", length = 30)
	public String getCiudad_envio_cliente() {
		return ciudad_envio_cliente;
	}

	public void setCiudad_envio_cliente(String ciudad_envio_cliente) {
		this.ciudad_envio_cliente = ciudad_envio_cliente;
	}

	@Column(name = "CP_ENVIO_CLIENTE", length = 5)
	public String getCp_envio_cliente() {
		return cp_envio_cliente;
	}

	public void setCp_envio_cliente(String cp_envio_cliente) {
		this.cp_envio_cliente = cp_envio_cliente;
	}

	@Column(name = "PROVINCIA_ENVIO_CLIENTE", length = 35)
	public String getProvincia_envio_cliente() {
		return provincia_envio_cliente;
	}

	public void setProvincia_envio_cliente(String provincia_envio_cliente) {
		this.provincia_envio_cliente = provincia_envio_cliente;
	}

	@Column(name = "TELEFONO_ENVIO_CLIENTE", length = 9)
	public String getTelefono_envio_cliente() {
		return telefono_envio_cliente;
	}

	public void setTelefono_envio_cliente(String telefono_envio_cliente) {
		this.telefono_envio_cliente = telefono_envio_cliente;
	}

	@Column(name = "FAX_ENVIO_CLIENTE", length = 9)
	public String getFax_envio_cliente() {
		return fax_envio_cliente;
	}

	public void setFax_envio_cliente(String fax_envio_cliente) {
		this.fax_envio_cliente = fax_envio_cliente;
	}

	@Column(name = "EMAIL_ENVIO_CLIENTE", length = 35)
	public String getEmail_envio_cliente() {
		return email_envio_cliente;
	}

	public void setEmail_envio_cliente(String email_envio_cliente) {
		this.email_envio_cliente = email_envio_cliente;
	}

	@Column(name = "OBSERVACIONES_CLIENTE", length = 2000)
	public String getObservaciones_cliente() {
		return observaciones_cliente;
	}

	public void setObservaciones_cliente(String observaciones_cliente) {
		this.observaciones_cliente = observaciones_cliente;
	}

	@Column(name = "ESTADO_CLIENTE", length = 2)
	public String getEstado_cliente() {
		return estado_cliente;
	}

	public void setEstado_cliente(String estado_cliente) {
		this.estado_cliente = estado_cliente;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cliente")
	public Set<Pedidos> getPedidoses() {
		return pedidoses;
	}

	public void setPedidoses(Set<Pedidos> pedidoses) {
		this.pedidoses = pedidoses;
	}

	@Override
	public String toString() {
		return "Clientes [codigo_cliente=" + codigo_cliente + ", nombre_cliente=" + nombre_cliente + ", tipo_empresa="
				+ tipo_empresa + ", cif_cliente=" + cif_cliente + ", nif_cliente=" + nif_cliente
				+ ", nombre_comercial_cliente=" + nombre_comercial_cliente + ", fecha_alta_cliente="
				+ fecha_alta_cliente + ", fecha_baja_cliente=" + fecha_baja_cliente + ", calle_cliente=" + calle_cliente
				+ ", ciudad_cliente=" + ciudad_cliente + ", cp_cliente=" + cp_cliente + ", provincia_cliente="
				+ provincia_cliente + ", telefono_cliente=" + telefono_cliente + ", fax_cliente=" + fax_cliente
				+ ", email_cliente=" + email_cliente + ", contado_cliente=" + contado_cliente + ", credito_cliente="
				+ credito_cliente + ", cifra_ventas_cliente=" + cifra_ventas_cliente + ", beneficios_cliente="
				+ beneficios_cliente + ", prepago_cliente=" + prepago_cliente + ", capital_cliente=" + capital_cliente
				+ ", inmovilizado_cliente=" + inmovilizado_cliente + ", pasivo_cliente=" + pasivo_cliente
				+ ", activo_cliente=" + activo_cliente + ", a�o_balance_cliente=" + a�o_balance_cliente
				+ ", limite_cliente=" + limite_cliente + ", pendiente_cliente=" + pendiente_cliente + ", resto_cliente="
				+ resto_cliente + ", calle_envio_cliente=" + calle_envio_cliente + ", ciudad_envio_cliente="
				+ ciudad_envio_cliente + ", cp_envio_cliente=" + cp_envio_cliente + ", provincia_envio_cliente="
				+ provincia_envio_cliente + ", telefono_envio_cliente=" + telefono_envio_cliente
				+ ", fax_envio_cliente=" + fax_envio_cliente + ", email_envio_cliente=" + email_envio_cliente
				+ ", observaciones_cliente=" + observaciones_cliente + ", estado_cliente=" + estado_cliente + "]";
	}
}
