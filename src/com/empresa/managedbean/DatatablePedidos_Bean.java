package com.empresa.managedbean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.*;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


import com.empresa.hibernate.Pedidos;
import com.empresa.hibernate.modelo.IGestion_Pedido;
import com.empresa.hibernate.Clientes;
import com.empresa.hibernate.modelo.IGestion_Cliente;

@ManagedBean(name="datatable_pedidos_bean")
@ViewScoped
public class DatatablePedidos_Bean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Pedidos> lista_pedidos;
	private List<Clientes> lista_clientes;

	private Pedidos pedido = new Pedidos();
	@ManagedProperty(value="#{gestion_pedido}")
	private IGestion_Pedido gestion_pedido;
	
	private Clientes cliente = new Clientes();
	@ManagedProperty(value="#{gestion_cliente}")
	private IGestion_Cliente gestion_cliente;
	
	private Long codigo_cliente;

	private int primera_filapagina;
	private int total_filas;
	
	private boolean modificar, crear;

    public boolean isModificar() {
        return modificar;
    }
    
    public boolean isCrear() {
    	return crear;
    }
    
	public DatatablePedidos_Bean() {

	}
	
	public Long getCodigo_cliente() {
		return codigo_cliente;
	}

	public void setCodigo_cliente(Long codigo_cliente) {
		this.codigo_cliente = codigo_cliente;
	}

	public Pedidos getPedido() {
		return pedido;
	}

	public void setPedido(Pedidos pedido) {
		this.pedido = pedido;
	}

	public Clientes getCliente() {
		return cliente;
	}

	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}

	@PostConstruct
	public void cargar_Pedidos() {
		// CARGA DE DATOS EN LA PROPIEDAD DEL MANAGEDBEAN
		try {
			lista_pedidos = gestion_pedido.consultar_Todos();
			lista_clientes = gestion_cliente.consultar_Todos();
			primera_filapagina = 0;
			total_filas = lista_pedidos.size();
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
		}
	}

	public void pagina_siguiente(ActionEvent evento) {
		if (!(primera_filapagina + 10 > total_filas)) {
			primera_filapagina += 10;
		}
	}

	public void pagina_anterior(ActionEvent evento) {
		if (!(primera_filapagina - 10 < 0)) {
			primera_filapagina -= 10;
		}
	}

	// ACCESORES PARA JSF
	public List<Pedidos> getLista_pedidos() {
		return lista_pedidos;
	}

	public void setLista_pedidos(List<Pedidos> lista_pedidos) {
		this.lista_pedidos = lista_pedidos;
	}
	public List<Clientes> getLista_clientes() {
		return lista_clientes;
	}

	public void setLista_clientes(List<Clientes> lista_clientes) {
		this.lista_clientes = lista_clientes;
	}


	public void setGestion_pedido(IGestion_Pedido gestion_pedido) {
		this.gestion_pedido = gestion_pedido;
	}

	public IGestion_Pedido getGestion_pedido() {
		return gestion_pedido;
	}

	public void setGestion_cliente(IGestion_Cliente gestion_cliente) {
		this.gestion_cliente = gestion_cliente;
	}

	public IGestion_Cliente getGestion_cliente() {
		return gestion_cliente;
	}

	public int getPrimera_filapagina() {
		return primera_filapagina;
	}

	public void setPrimera_filapagina(int primera_filapagina) {
		this.primera_filapagina = primera_filapagina;
	}

	public void crear() {
		try {
        	this.cliente = gestion_cliente.consultar_PorCodigo(this.codigo_cliente);
        	this.pedido.setCliente(cliente);
			this.gestion_pedido.alta_Pedido(this.pedido);
			this.pedido = new Pedidos();
			lista_pedidos = gestion_pedido.consultar_Todos();
			this.crear = false;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
		}
	}
	
	public void crear_pedido() {
		this.codigo_cliente = null;
		this.pedido = new Pedidos();
		this.cliente = new Clientes();
		this.pedido.setCliente(cliente);
		this.crear = true;
	}
	
	public boolean cambios() {
		return !this.modificar && !this.crear;
	}

	public void eliminar(Pedidos pedido) {
		try {
			this.gestion_pedido.baja_Pedido(pedido);
			lista_pedidos = gestion_pedido.consultar_Todos();
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
		}
	}
	
    public void modificar(Pedidos pedido) {
    	try {
    		this.codigo_cliente = pedido.getCliente().getCodigo_cliente();
        	this.cliente = gestion_cliente.consultar_PorCodigo(this.codigo_cliente);
        	pedido.setCliente(cliente);
            this.pedido = pedido;
			lista_pedidos = gestion_pedido.consultar_Todos();
            this.modificar = true;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
		}
    }

    public void modificar() {
		try {
        	this.cliente = gestion_cliente.consultar_PorCodigo(this.codigo_cliente);
        	this.pedido.setCliente(cliente);
			this.gestion_pedido.modificion_Pedido(this.pedido);
	        this.pedido = new Pedidos();  
			lista_pedidos = gestion_pedido.consultar_Todos();
	        this.modificar = false;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
		}
    }
    
    public void cambiar() {
        this.modificar = false;
		this.crear = false;
    }
}