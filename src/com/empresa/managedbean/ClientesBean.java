package com.empresa.managedbean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.empresa.hibernate.Clientes;
import com.empresa.hibernate.modelo.IGestion_Cliente;

@ManagedBean(name="clientes_bean")
@ViewScoped
public class ClientesBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Clientes cliente = new Clientes();
    private List<Clientes> lstClientes;
    private String accion;
	@ManagedProperty(value="#{gestion_cliente}")
	private IGestion_Cliente gestion_cliente;

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.limpiar();
        this.accion = accion;
    }

    public List<Clientes> getLstClientes() {
        return lstClientes;
    }

    public void setLstClientes(List<Clientes> lstClientes) {
        this.lstClientes = lstClientes;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }
    
    public IGestion_Cliente getGestion_cliente() {
		return gestion_cliente;
	}

	public void setGestion_cliente(IGestion_Cliente gestion_cliente) {
		this.gestion_cliente = gestion_cliente;
	}

	private boolean isPostBack(){
        boolean rpta;
        rpta = FacesContext.getCurrentInstance().isPostback();
        return rpta;
    }
    
    public void operar() throws Exception{
        switch(accion){
            case "Registrar":
                this.registrar();
                this.limpiar();
                break;
            case "Modificar":
                this.modificar();
                this.limpiar();
                break;
        }
    }
    
    public void limpiar(){
        this.cliente = new Clientes();
    }
    
    private void registrar() throws Exception{
        try {
        	gestion_cliente.alta_Cliente(cliente);
            this.listar("V");
        } catch (Exception e) {
            throw e;
        }
    }

    private void modificar() throws Exception{        
        try {
        	gestion_cliente.modificion_Cliente(cliente);
            this.listar("V");
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void listar(String valor) throws Exception{
        try {
            if(valor.equals("F")){
                if(isPostBack() == false){
                    lstClientes = gestion_cliente.consultar_Todos();
                }
            } else {
                    lstClientes = gestion_cliente.consultar_Todos();
            }
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void leerID(Clientes cli) throws Exception{
        Clientes temp;
        
        try {
            temp = gestion_cliente.consultar_PorCodigo(cli.getCodigo_cliente());
            if(temp != null){
                this.cliente = temp;
                this.accion = "Modificar";
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void eliminar(Clientes clie) throws Exception{
        try {
        	gestion_cliente.baja_Cliente(clie);
            this.listar("V");
        } catch (Exception e) {
            throw e;
        }
    }
}