package com.empresa.managedbean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.*;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


import com.empresa.hibernate.Clientes;
import com.empresa.hibernate.modelo.IGestion_Cliente;

@ManagedBean(name="datatable_clientes_bean")
@ViewScoped
public class DatatableClientes_Bean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Clientes> lista_clientes;
	private Clientes cliente = new Clientes();
	@ManagedProperty(value="#{gestion_cliente}")
	private IGestion_Cliente gestion_cliente;

	private int primera_filapagina;
	private int total_filas;
	
	private boolean modificar, crear;

    public boolean isModificar() {
        return modificar;
    }
    
    public boolean isCrear() {
    	return crear;
    }
    
	public DatatableClientes_Bean() {

	}

	public Clientes getCliente() {
		return cliente;
	}

	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}

	@PostConstruct
	public void cargar_Clientes() {
		// CARGA DE DATOS EN LA PROPIEDAD DEL MANAGEDBEAN
		try {
			lista_clientes = gestion_cliente.consultar_Todos();
			primera_filapagina = 0;
			total_filas = lista_clientes.size();
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
		}
	}

	public void pagina_siguiente(ActionEvent evento) {
		if (!(primera_filapagina + 10 > total_filas)) {
			primera_filapagina += 10;
		}
	}

	public void pagina_anterior(ActionEvent evento) {
		if (!(primera_filapagina - 10 < 0)) {
			primera_filapagina -= 10;
		}
	}

	// ACCESORES PARA JSF
	public List<Clientes> getLista_clientes() {
		return lista_clientes;
	}

	public void setLista_clientes(List<Clientes> lista_clientes) {
		this.lista_clientes = lista_clientes;
	}

	public void setGestion_cliente(IGestion_Cliente gestion_cliente) {
		this.gestion_cliente = gestion_cliente;
	}

	public IGestion_Cliente getGestion_cliente() {
		return gestion_cliente;
	}

	public int getPrimera_filapagina() {
		return primera_filapagina;
	}

	public void setPrimera_filapagina(int primera_filapagina) {
		this.primera_filapagina = primera_filapagina;
	}

	public void crear() {
		try {
			this.gestion_cliente.alta_Cliente(this.cliente);
			this.cliente = new Clientes();
			lista_clientes = gestion_cliente.consultar_Todos();
			this.crear = false;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
		}
	}
	
	public void crear_cliente() {
		this.cliente = new Clientes();
		this.crear = true;
	}
	
	public boolean cambios() {
		return !this.modificar && !this.crear;
	}

	public void eliminar(Clientes cliente) {
		try {
			this.gestion_cliente.baja_Cliente(cliente);
			lista_clientes = gestion_cliente.consultar_Todos();
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
		}
	}
	
    public void modificar(Clientes cliente) {
        this.cliente = cliente;
        this.modificar = true;
    }

    public void modificar() {
		try {
			this.gestion_cliente.modificion_Cliente(this.cliente);
	        this.cliente = new Clientes();  
	        this.modificar = false;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
		}
    }
    
    public void cambiar() {
        this.modificar = false;
		this.crear = false;
    }
}