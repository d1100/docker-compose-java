package com.empresa.managedbean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.*;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;


import com.empresa.hibernate.Articulos;
import com.empresa.hibernate.modelo.IGestion_Articulo;

@ManagedBean(name="datatable_articulos_bean")
@ViewScoped
public class DatatableArticulos_Bean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Articulos> lista_articulos;
	private Articulos articulo = new Articulos();
	@ManagedProperty(value="#{gestion_articulo}")
	private IGestion_Articulo gestion_articulo;

	private int primera_filapagina;
	private int total_filas;
	
	private boolean modificar, crear;

    public boolean isModificar() {
        return modificar;
    }
    
    public boolean isCrear() {
    	return crear;
    }
    
	public DatatableArticulos_Bean() {

	}

	public Articulos getArticulo() {
		return articulo;
	}

	public void setArticulo(Articulos articulo) {
		this.articulo = articulo;
	}

	@PostConstruct
	public void cargar_Articulos() {
		// CARGA DE DATOS EN LA PROPIEDAD DEL MANAGEDBEAN
		try {
			lista_articulos = gestion_articulo.consultar_Todos();
			primera_filapagina = 0;
			total_filas = lista_articulos.size();
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
		}
	}

	public void pagina_siguiente(ActionEvent evento) {
		if (!(primera_filapagina + 10 > total_filas)) {
			primera_filapagina += 10;
		}
	}

	public void pagina_anterior(ActionEvent evento) {
		if (!(primera_filapagina - 10 < 0)) {
			primera_filapagina -= 10;
		}
	}

	// ACCESORES PARA JSF
	public List<Articulos> getLista_articulos() {
		return lista_articulos;
	}

	public void setLista_articulos(List<Articulos> lista_articulos) {
		this.lista_articulos = lista_articulos;
	}

	public void setGestion_articulo(IGestion_Articulo gestion_articulo) {
		this.gestion_articulo = gestion_articulo;
	}

	public IGestion_Articulo getGestion_articulo() {
		return gestion_articulo;
	}

	public int getPrimera_filapagina() {
		return primera_filapagina;
	}

	public void setPrimera_filapagina(int primera_filapagina) {
		this.primera_filapagina = primera_filapagina;
	}

	public void crear() {
		try {
			this.gestion_articulo.alta_Articulo(this.articulo);
			this.articulo = new Articulos();
			lista_articulos = gestion_articulo.consultar_Todos();
			this.crear = false;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
		}
	}
	
	public void crear_articulo() {
		this.articulo = new Articulos();
		this.crear = true;
	}
	
	public boolean cambios() {
		return !this.modificar && !this.crear;
	}

	public void eliminar(Articulos articulo) {
		try {
			this.gestion_articulo.baja_Articulo(articulo);
			lista_articulos = gestion_articulo.consultar_Todos();
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
		}
	}
	
    public void modificar(Articulos articulo) {
        this.articulo = articulo;
        this.modificar = true;
    }

    public void modificar() {
		try {
			this.gestion_articulo.modificion_Articulo(this.articulo);
	        this.articulo = new Articulos();  
	        this.modificar = false;
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.getMessage()));
		}
    }
    
    public void cambiar() {
        this.modificar = false;
		this.crear = false;
    }
}