package com.empresa.managedbean;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.empresa.hibernate.Articulos;
import com.empresa.hibernate.modelo.IGestion_Articulo;

@ManagedBean(name="articulos_bean")
@ViewScoped
public class ArticulosBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Articulos articulo = new Articulos();
    private List<Articulos> lstArticulos;
    private String accion;
	@ManagedProperty(value="#{gestion_articulo}")
	private IGestion_Articulo gestion_articulo;

	public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.limpiar();
        this.accion = accion;
    }

    public List<Articulos> getLstArticulos() {
        return lstArticulos;
    }

    public void setLstArticulos(List<Articulos> lstArticulos) {
        this.lstArticulos = lstArticulos;
    }

    public Articulos getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulos articulo) {
        this.articulo = articulo;
    }
    
    public IGestion_Articulo getGestion_articulo() {
		return gestion_articulo;
	}

	public void setGestion_articulo(IGestion_Articulo gestion_articulo) {
		this.gestion_articulo = gestion_articulo;
	}

	private boolean isPostBack(){
        boolean rpta;
        rpta = FacesContext.getCurrentInstance().isPostback();
        return rpta;
    }
    
    public void operar() throws Exception{
        switch(accion){
            case "Registrar":
                this.registrar();
                this.limpiar();
                break;
            case "Modificar":
                this.modificar();
                this.limpiar();
                break;
        }
    }
    
    public void limpiar(){
        this.articulo.setCodigo_articulo(0L);
        this.articulo.setDescripcion_articulo("");
        this.articulo.setPrecio_unidad_articulo(0.0);
        this.articulo.setCantidad(0);
    }
    
    private void registrar() throws Exception{
        try {
        	gestion_articulo.alta_Articulo(articulo);
            this.listar("V");
        } catch (Exception e) {
            throw e;
        }
    }

    private void modificar() throws Exception{
        try {
        	gestion_articulo.modificion_Articulo(articulo);
            this.listar("V");
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void listar(String valor) throws Exception{
        try {
            if(valor.equals("F")){
                if(isPostBack() == false){
                    lstArticulos = gestion_articulo.consultar_Todos();
                }
            } else {
                    lstArticulos = gestion_articulo.consultar_Todos();
            }
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void leerID(Articulos art) throws Exception{
        Articulos temp;
        
        try {
            temp = gestion_articulo.consultar_PorCodigo(art.getCodigo_articulo());
            if(temp != null){
                this.articulo = temp;
                this.accion = "Modificar";
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void eliminar(Articulos arti) throws Exception{
        try {
        	gestion_articulo.baja_Articulo(arti);
            this.listar("V");
        } catch (Exception e) {
            throw e;
        }
    }
}