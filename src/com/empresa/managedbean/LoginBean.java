package com.empresa.managedbean;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.empresa.hibernate.Usuarios;
import com.empresa.hibernate.modelo.IGestion_usuario;

@ManagedBean(name = "login_bean")
@ViewScoped
public class LoginBean implements Serializable {

	private static final long serialVersionUID = 1L;
	// PROPIEDADES PARA RECIBIR LOS VALORES DE LOS COMPONENTES
	private String nombre;
	private String clave;
	private Usuarios usuario;
	// FACHADA DE LA CAPA MODELO
	@ManagedProperty("#{gestion_usuario}")
	private IGestion_usuario gestion_usuario;

	@PostConstruct
	public void init() {
		usuario = new Usuarios();
	}

	public String iniciarSesion() {
		String redireccion = null;
		try {
			Usuarios us;
			us = gestion_usuario.iniciarSesion(usuario);
			if(us != null) {
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", us);
				redireccion = "principal?faces-redirect=true";
			} else {
	            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Aviso","Credenciales incorrectas"));
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Error",e.getMessage()));
		}
		return redireccion;
	}
	
	public void verificarSesion() {
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			Usuarios us = (Usuarios) context.getExternalContext().getSessionMap().get("usuario");
			if(us == null) {
				context.getExternalContext().redirect("login.xhtml");
				context.responseComplete();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void cerrarSesion() {
		try {
			usuario = null;
			FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
			FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
			FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// ACCESORES PARA JSF
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public Usuarios getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuarios usuario) {
		this.usuario = usuario;
	}


	// ACCESORES PARA SPRING
	public void setGestion_usuario(IGestion_usuario gestion_usuario) {
		this.gestion_usuario = gestion_usuario;
	}

	public IGestion_usuario getGestion_usuario() {
		return gestion_usuario;
	}
}