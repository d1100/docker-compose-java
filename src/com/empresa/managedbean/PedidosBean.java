package com.empresa.managedbean;

import com.empresa.hibernate.Articulos;
import com.empresa.hibernate.Linea_Pedido;
import com.empresa.hibernate.Pedidos;
import com.empresa.hibernate.Clientes;
import com.empresa.hibernate.modelo.IGestion_Pedido;
import com.empresa.hibernate.modelo.IGestion_Linea_Pedido;
import com.empresa.hibernate.modelo.IGestion_Articulo;
import com.empresa.hibernate.modelo.IGestion_Cliente;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name="pedidos_bean")
@ViewScoped
public class PedidosBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private Pedidos pedido = new Pedidos();
    private Articulos articulo = new Articulos();
    private Clientes cliente = new Clientes();
    private Linea_Pedido linea_pedido = new Linea_Pedido();
    private Integer numero_unidades_articulo;
    private Date fecha_pedido;
    private List<Linea_Pedido> lista = new ArrayList<Linea_Pedido>();
    private List<Pedidos> lstPedidos;
    private String accion;
    private Double total;
	@ManagedProperty(value="#{gestion_pedido}")
	private IGestion_Pedido gestion_pedido;
	@ManagedProperty(value="#{gestion_linea_pedido}")
	private IGestion_Linea_Pedido gestion_linea_pedido;
	@ManagedProperty(value="#{gestion_articulo}")
	private IGestion_Articulo gestion_articulo;
	@ManagedProperty(value="#{gestion_cliente}")
	private IGestion_Cliente gestion_cliente;

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
        this.limpiar();
    }

    public List<Pedidos> getLstPedidos() {
        return lstPedidos;
    }

    public void setLstPedidos(List<Pedidos> lstPedidos) {
        this.lstPedidos = lstPedidos;
    }

    public Clientes getCliente() {
		return cliente;
	}

	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}

	public Linea_Pedido getLinea_pedido() {
		return linea_pedido;
	}

	public void setLinea_pedido(Linea_Pedido linea_pedido) {
		this.linea_pedido = linea_pedido;
	}

	public Pedidos getPedido() {
        return pedido;
    }

    public void setPedido(Pedidos pedido) {
        this.pedido = pedido;
    }

    public Articulos getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulos articulo) {
        this.articulo = articulo;
    }

    public Integer getNumero_unidades_articulo() {
        return numero_unidades_articulo;
    }

    public void setNumero_unidades_articulo(Integer numero_unidades_articulo) {
        this.numero_unidades_articulo = numero_unidades_articulo;
    }

    public Date getFecha_pedido() {
		return fecha_pedido;
	}

	public void setFecha_pedido(Date fecha_pedido) {
		this.fecha_pedido = fecha_pedido;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public List<Linea_Pedido> getLista() {
        return lista;
    }

    public void setLista(List<Linea_Pedido> lista) {
        this.lista = lista;
    }
    
    public IGestion_Pedido getGestion_pedido() {
		return gestion_pedido;
	}

	public void setGestion_pedido(IGestion_Pedido gestion_pedido) {
		this.gestion_pedido = gestion_pedido;
	}

	public IGestion_Linea_Pedido getGestion_linea_pedido() {
		return gestion_linea_pedido;
	}

	public void setGestion_linea_pedido(IGestion_Linea_Pedido gestion_linea_pedido) {
		this.gestion_linea_pedido = gestion_linea_pedido;
	}

	public IGestion_Articulo getGestion_articulo() {
		return gestion_articulo;
	}

	public void setGestion_articulo(IGestion_Articulo gestion_articulo) {
		this.gestion_articulo = gestion_articulo;
	}

	public IGestion_Cliente getGestion_cliente() {
		return gestion_cliente;
	}

	public void setGestion_cliente(IGestion_Cliente gestion_cliente) {
		this.gestion_cliente = gestion_cliente;
	}

	private boolean isPostBack(){
        boolean rpta;
        rpta = FacesContext.getCurrentInstance().isPostback();
        return rpta;
    }
    
    public void operar() throws Exception{
        switch(accion){
            case "Registrar":
                this.registrar();
                this.limpiar();
                break;
            case "Modificar":
                this.modificar();
                this.limpiar();
                break;
        }
    }
    
    public void limpiar(){
        this.pedido = new Pedidos();
        this.cliente = new Clientes();
        this.lista = new ArrayList<Linea_Pedido>();
        this.fecha_pedido = new Date();
        this.total = 0.0;
        if(accion.equals("Registrar"))
        	this.pedido.setNumero_pedido((gestion_pedido.getMax()!=null?gestion_pedido.getMax():0)+1);
    }

	public void agregar(){
        Linea_Pedido lp = new Linea_Pedido();
        lp.setNumero_unidades_articulo(numero_unidades_articulo);
        lp.setArticulo(articulo);
        lp.setPrecio_unidad_articulo(articulo.getPrecio_unidad_articulo());
        total += articulo.getPrecio_unidad_articulo() * numero_unidades_articulo;
        this.lista.add(lp);
    }
	
	public void borrar(Linea_Pedido lp) {
        total -= lp.getPrecio_unidad_articulo() * lp.getNumero_unidades_articulo();
		this.lista.remove(lp);
	}
    
    public void registrar() throws Exception{
        double monto = 0.0;
        Long numero_pedido = gestion_pedido.getMax()!=null?gestion_pedido.getMax():0;
        Long codigo_linea_pedido = gestion_linea_pedido.getMax()!=null?gestion_linea_pedido.getMax():0;
        try {
            for(Linea_Pedido det : lista){
                monto += det.getArticulo().getPrecio_unidad_articulo()*det.getNumero_unidades_articulo();
            }
            pedido.setCliente(cliente);
            pedido.setTotal_factura_pedido(monto);
            pedido.setPorcentaje_iva_pedido(21);
            pedido.setOtros_cargos_pedido(0.0);
            pedido.setTotal_cargos_pedido(0.0);
            pedido.setPorte_pedido(0.0);
            pedido.setSeguro_pedido(0.0);
            pedido.setIva_pedido(monto*pedido.getPorcentaje_iva_pedido()/100);
            pedido.setTotal_bruto_pedido(pedido.getTotal_factura_pedido()-pedido.getIva_pedido());
            pedido.setNumero_pedido((Long)numero_pedido+1);
            pedido.setFecha_pedido(this.fecha_pedido);
            gestion_pedido.alta_Pedido(pedido);
            
            for(Linea_Pedido det : lista) {
                codigo_linea_pedido++;
                det.setCodigo_linea_pedido(codigo_linea_pedido);
                det.setPedido(pedido);
                gestion_linea_pedido.alta_Linea_Pedido(det);
            }
            pedido.setLinea_pedidoses(new HashSet<>(lista));
            gestion_pedido.modificion_Pedido(pedido);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Aviso","El pedido se guardo correctamente"));
        } catch (Exception e) {
        	e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Error",e.getMessage()));
        }finally{
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        }
    }

    private void modificar() throws Exception{        
        try {
            double monto = 0.0;
            for(Linea_Pedido det : lista){
                monto += det.getArticulo().getPrecio_unidad_articulo()*det.getNumero_unidades_articulo();
            }
            pedido.setCliente(cliente);
            pedido.setTotal_factura_pedido(monto);
            pedido.setPorcentaje_iva_pedido(21);
            pedido.setOtros_cargos_pedido(0.0);
            pedido.setTotal_cargos_pedido(0.0);
            pedido.setPorte_pedido(0.0);
            pedido.setSeguro_pedido(0.0);
            pedido.setIva_pedido(monto*pedido.getPorcentaje_iva_pedido()/100);
            pedido.setTotal_bruto_pedido(pedido.getTotal_factura_pedido()-pedido.getIva_pedido());
            pedido.setFecha_pedido(this.fecha_pedido);

        	borrarDetalle(pedido.getNumero_pedido());
        	Long codigo_linea_pedido = gestion_linea_pedido.getMax()!=null?gestion_linea_pedido.getMax():0;
            for(Linea_Pedido det : lista) {
            	codigo_linea_pedido++;
                det.setPedido(pedido);
            	det.setCodigo_linea_pedido(codigo_linea_pedido);
                gestion_linea_pedido.alta_Linea_Pedido(det);
            }
        	pedido.setLinea_pedidoses(new HashSet<Linea_Pedido>(lista));
        	gestion_pedido.modificion_Pedido(pedido);
            this.listar("V");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Aviso","El pedido se modific� correctamente"));
        } catch (Exception e) {
        	e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Error",e.getMessage()));
        }finally{
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        }
    }
    
    private void borrarDetalle(Long numero_pedido) {
    	List<Linea_Pedido> temp = gestion_linea_pedido.consultar_Lista(numero_pedido);
        for(Linea_Pedido det : temp){
            gestion_linea_pedido.baja_Linea_Pedido(det);
        }
    }
    
    public void listar(String valor) throws Exception{
        try {
            if(valor.equals("F")){
                if(isPostBack() == false){
                    lstPedidos = gestion_pedido.consultar_Todos();
                }
            } else {
                    lstPedidos = gestion_pedido.consultar_Todos();
            }
        } catch (Exception e) {
            throw e;
        }
    }
    
    public void leerID(Pedidos ped) throws Exception{
        try {
        	this.lista = gestion_linea_pedido.consultar_Lista(ped.getNumero_pedido());
        	this.cliente = gestion_cliente.consultar_PorCodigo(ped.getCliente().getCodigo_cliente());
        	ped.setCliente(cliente);
        	this.total = 0.0;
            this.pedido = ped;
            this.fecha_pedido = ped.getFecha_pedido();
        	List<Linea_Pedido> lstLinea_Pedido = new ArrayList<Linea_Pedido>();
            for(Linea_Pedido det : lista){
                this.articulo = gestion_articulo.consultar_PorCodigo(det.getArticulo().getCodigo_articulo());
                if(det.getNumero_unidades_articulo()==null) {
                	det.setNumero_unidades_articulo(0);
                }
                det.setArticulo(this.articulo);
                total += this.articulo.getPrecio_unidad_articulo() * det.getNumero_unidades_articulo();
                lstLinea_Pedido.add(det);
            }
            this.lista = lstLinea_Pedido;
            this.accion = "Modificar";
        } catch (Exception e) {
            throw e;
        }
    }

    public void eliminar(Pedidos pedi) throws Exception{
        try {
        	gestion_pedido.baja_Pedido(pedi);
            this.listar("V");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Aviso","El pedido ha sido eliminado"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL,"Error",e.getMessage()));
        }finally{
            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        }
    }
}