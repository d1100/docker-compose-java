FROM tomcat:8.5.47-jdk8-openjdk
# FROM ubuntu:20.04
# RUN apt update
# RUN mkdir -p /usr/lib/jvm
# COPY jdk-8u301-linux-x64.tar/jdk-8u301-linux-x64.tar /usr/lib/jvm

# RUN update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0_301/bin/java" 0
# RUN update-alternatives --set java /usr/lib/jvm/jdk1.8.0_301/bin/java

COPY webclient.war /usr/local/tomcat/webapps
# EXPOSE 8080

# wget http://www.apache.org/dist/tomcat/tomcat-8/v8.5.70/bin/apache-tomcat-8.5.70.zip
# https://linuxize.com/post/how-to-install-tomcat-8-5-on-ubuntu-18-04/