## Build the dockerfile:
```
docker build -f Dockerfile -t my-web-app .
```
## Run the application by executing this command:
```
docker run --rm -p 8080:4200 my-web-app
```
